#include "bsg_pci_host.h"


bsg_pci_device::bsg_pci_device(const char *device)
{
    open_device(device);

    in_list = new vector<list<unsigned int> >(channel_number);

    verify_iom(in_list != NULL, (stderr, "Error allocating in_list\n"));

    current_channel = 0;

    initiate_connection();

    myfile.open("bsg_pci_dump");
}

bsg_pci_device::~bsg_pci_device()
{
    delete in_list;
    delete l2f_status_reg;

    munmap(bpd_base_addr, 0x00001000);

    close(fd);

    myfile.close();
}

// Only used for read (from FPGA to Linux)
void bsg_pci_device::handle_pci_io(int channel)
{
    if(DEBUG) {
        printf("I am in the handle_pci_io with one argus channel number is %d\n", channel);
    }

    int packet_number = *(bpd_channel[channel].bsg_pci_f2l_status_reg);

    for (int i = 0; i < packet_number; i++) {
        (*in_list)[channel].push_back((*(bpd_channel[channel].bsg_pci_f2l_fifo)));
    }
}

int bsg_pci_device::write_packet_if_less_than (int channel, unsigned int t, int n)
{
    if(DEBUG) {
        printf("I am in the write_packet_if_less_than \n");
    }

    if ((*l2f_status_reg)[channel] > 0) {

        write_packet_async(channel, t);

        return 1;
    }
    else {

        (*l2f_status_reg)[channel] = *(bpd_channel[channel].bsg_pci_l2f_status_reg);

        if ((*l2f_status_reg)[channel] > 0) {

            write_packet_async(channel, t);

            return 1;
        }

        return 0;
    }
}

void bsg_pci_device::write_packet_async (int channel, unsigned int t)
{
    verify_iom(channel < channel_number, (stderr, "Error writing to invalid channel %d\n", channel));

    if(DEBUG) {
        if (channel == 0) {
            myfile << "*** Data in to static_network of West, the value is " << setw(8) << setfill('0') << hex << t << ", on cycle = " << endl;
        }
        else if (channel == 1) {
            myfile << "*** Data in to general_network of West, the value is " << setw(8) << setfill('0') << hex << t << ", on cycle = " << endl;
        }
        else if (channel == 2) {
            myfile << "*** Data in to memory_network of West, the value is " << setw(8) << setfill('0') << hex << t << ", on cycle = " << endl;
        }
        else if (channel == 3) {
            myfile << "*** Data in to static_network of East, the value is " << setw(8) << setfill('0') << hex << t << ", on cycle = " << endl;
        }
        else if (channel == 4) {
            myfile << "*** Data in to general_network of East, the value is " << setw(8) << setfill('0') << hex << t << ", on cycle = " << endl;
        }
        else if (channel == 5) {
            myfile << "*** Write to test network, totally wrong" << endl;
        }
        else {
            myfile << "Channel number out of range" << endl;
        }
    }
    *(bpd_channel[channel].bsg_pci_l2f_fifo) = t;
}


int bsg_pci_device::read_packet_async (int channel, unsigned int &t)
{
    if ((*in_list)[channel].empty()) {

        handle_pci_io(channel);

        if (!(*in_list)[channel].empty()) {
            t = (*in_list)[channel].front();
            (*in_list)[channel].pop_front();

            return 1;
        }

        return 0;
    }
    else {

        t = (*in_list)[channel].front();
        (*in_list)[channel].pop_front();

        return 1;
    }
}

void bsg_pci_device::read_packet_blocking (int channel, unsigned int &t, int nicely)
{
    while (1) {
        if(!read_packet_async(channel, t) && nicely)
            check_after_sleep(1);
        else
            break;
    }
}

int bsg_pci_device::peek_packet_async (int channel, unsigned int &t)
{
    if ((*in_list)[channel].empty()) {

        handle_pci_io(channel);

        if (!(*in_list)[channel].empty()) {
            t = (*in_list)[channel].front();
            return 1;
        }

        return 0;
    }
    else {

        t = (*in_list)[channel].front();

        return 1;
    }
}

void bsg_pci_device::open_device(const char *device) 
{
    fd = open(device, O_RDWR);
    bpd_base_addr = mmap(0, 0x00001000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    bpd_channel_number = (unsigned int *) ((long int)bpd_base_addr + BSG_PCI_CHANNEL_NUMBER);
    bpd_host_reset     = (unsigned int *) ((long int)bpd_base_addr + BSG_PCI_HOST_RESET);
    bpd_test_reg       = (unsigned int *) ((long int)bpd_base_addr + BSG_PCI_TEST_REG);
    bpd_status_reg     = (unsigned int *) ((long int)bpd_base_addr + BSG_PCI_STATUS_REG);

    // Send the reset request
    *bpd_host_reset = 0xffffffff;

    printf("BSG PCI status reg: 0x%08x\n", (*bpd_status_reg));

    int i = 0;
    while ((*bpd_status_reg)  != 0x0000ffff) { i = i + 10; };

    // Get the channel number from the hardware
    channel_number = *bpd_channel_number;

    printf("Channel Number = 0x%08x \n", channel_number);

    bpd_channel = (bsg_pci_channel *) malloc (channel_number * sizeof(bsg_pci_channel));

    for (i = 0; i < channel_number; i++) {

        bpd_channel[i].bsg_pci_l2f_status_reg = (unsigned int *) ((long int)bpd_base_addr + 0x4 * i);
        bpd_channel[i].bsg_pci_l2f_fifo  = (unsigned int *) ((long int)bpd_base_addr + 0x40 + 0x4 * i);
        bpd_channel[i].bsg_pci_f2l_status_reg = (unsigned int *) ((long int)bpd_base_addr + 0xc0 + 0x4 * i);
        bpd_channel[i].bsg_pci_f2l_fifo  = (unsigned int *) ((long int)bpd_base_addr + 0x80 + 0x4 * i);
    }

    l2f_status_reg = new vector<int> (channel_number);

    for (i = 0; i < channel_number; i++) {

        (*l2f_status_reg)[i] = *(bpd_channel[i].bsg_pci_l2f_status_reg);

        printf("Linux to FPGA status regs for channel %d is = 0x%08x\n", i, (*l2f_status_reg)[i]);
    }

    printf("BSG PCI status reg: 0x%08x\n", (*bpd_status_reg));
}


void bsg_pci_device::initiate_connection() 
{
    // Read and write the test register
    if (*bpd_test_reg != 0xfffffff0) {
        printf("Maybe something wrong with reset the FPGA board!\n");
    }

    *bpd_test_reg = 0xf0ffff5c;

    if (*bpd_test_reg != 0xf0ffff5c) {
        printf("Maybe something wrong with write and read the FPGA board!\n");
    }

    printf("BSG PCI device is ready to work!\n");
}


int bsg_pci_device::check_after_sleep(int usec) 
{
    return 1;
}

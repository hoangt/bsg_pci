#include "bsg_pci_host.h"

int main (int argc, char *argv[])
{
    char * device = kPCIE_DEVICE;
    class bsg_pci_device bpd (device);
    unsigned int receive;
    int read_flag, i;

    bpd.write_packet_if_less_than(0, 0x00010002, 10);
    bpd.write_packet_if_less_than(0, 0x00030004, 10);
    bpd.write_packet_if_less_than(0, 0x00050006, 10);
    bpd.write_packet_if_less_than(0, 0x00070008, 10);
    bpd.write_packet_if_less_than(0, 0x0009000a, 10);
    bpd.write_packet_if_less_than(0, 0x000b000c, 10);
    bpd.write_packet_if_less_than(0, 0x000d000e, 10);
    bpd.write_packet_if_less_than(0, 0x000f0000, 10);

    printf("Finished to write on channel 0 \n");

    for (i = 0; i < 8;) {
        
        read_flag = bpd.read_packet_async(0, receive); 
       
        if (read_flag == 1) {
            printf("Received data = 0x%08x\n", receive);
            i++;
        }
        else {
            printf("ERROR: Something is wrong, read_flag is 0\n");
        }

    }

    printf("Finished Channel 0 test\n");
    
    bpd.write_packet_if_less_than(1, 0x10011002, 10);
    bpd.write_packet_if_less_than(1, 0x10031004, 10);
    bpd.write_packet_if_less_than(1, 0x10051006, 10);
    bpd.write_packet_if_less_than(1, 0x10071008, 10);
    bpd.write_packet_if_less_than(1, 0x1009100a, 10);
    bpd.write_packet_if_less_than(1, 0x100b100c, 10);
    bpd.write_packet_if_less_than(1, 0x100d100e, 10);
    bpd.write_packet_if_less_than(1, 0x100f1000, 10);

    printf("Finished to write on channel 1 \n");

    for (i = 0; i < 8;) {
        
        read_flag = bpd.read_packet_async(1, receive); 

        if (read_flag == 1) {
            printf("Received data = 0x%08x\n", receive);
            i++;
        }
        else {
            printf("ERROR: Something is wrong, read_flag is 0\n");
        }

    }
    
    printf("Finished Channel 1 test\n");

    return 0;
}

#include "bsg_pci.h"

char* bsg_pci_drivername;

static pci_ers_result_t bsg_pci_pci_error_detected(struct pci_dev *dev, enum pci_channel_state error)
{
    BSG_DEBUG("PCI bus error detected");
    return PCI_ERS_RESULT_NONE;
}

struct pci_device_id bsg_pci_ids[] = {
    { PCI_DEVICE(BSG_PCI_DEVICE_MFG, BSG_PCI_DEVICE_ID), },
    { 0, }
};

struct pci_error_handlers bsg_pci_driver_pci_eh = {
    .error_detected = bsg_pci_pci_error_detected,
};

struct pci_driver bsg_pci_driver = {
    .name = "bsg_pci",
    .id_table = bsg_pci_ids,
    .probe = bsg_pci_probe,
    .remove = bsg_pci_remove,
    .err_handler = &bsg_pci_driver_pci_eh,
};

//--------------------------------------------------------------
//                      KERNEL MODULE INFO
//--------------------------------------------------------------
MODULE_DEVICE_TABLE(pci, bsg_pci_ids);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Bespoke Systems Group UCSD");
//--------------------------------------------------------------

//---------------------------------
// KERNEL-INIT FUNCTION STARTS HERE
//---------------------------------

static int __init bsg_pci_init(void)
{
    int ret;

    bsg_pci_drivername = bsg_pci_driver.name;
    ret = pci_register_driver(&bsg_pci_driver);  
    if (ret == 0) {
        BSG_PCI_INFO("Driver Registered");
        BSG_PCI_INFO("Using PCIE ID: %.4x:%.4x", bsg_pci_ids[0].vendor, bsg_pci_ids[0].device);  
    } else {
        BSG_PCI_INFO("Driver Registration Error: %d", ret);
        return ret;
    }

    return 0;
}

//-------------------------------
// KERNEL-INIT FUNCTION ENDS HERE
//-------------------------------

//---------------------------------
// KERNEL-EXIT FUNCTION STARTS HERE
//---------------------------------

static void __exit bsg_pci_exit(void)
{
    pci_unregister_driver(&bsg_pci_driver);
    BSG_PCI_INFO("Module Unloaded");
}

//-------------------------------
// KERNEL-EXIT FUNCTION ENDS HERE
//-------------------------------

//--------------------------------
// KERNEL MACROS FOR INIT AND EXIT
//--------------------------------
module_init(bsg_pci_init);
module_exit(bsg_pci_exit);
//--------------------------------

//-------------------------------
// PCI-PROBE FUNCTION STARTS HERE
//-------------------------------

// Called when a new pci device is found that matches the device ID and 
// manufacturer ID we specified. This is responsible for getting the device 
// to a usable state and registering any entries in /dev.

int __devinit bsg_pci_probe(struct pci_dev *dev, const struct pci_device_id *id)
{
    return  bsg_pci_alloc(dev);
}

int bsg_pci_alloc(struct pci_dev *pcidev)
{
    struct bsg_pci_device *BSG_PCI_DEV;
    int rc;

    BSG_DEBUG("Initializing device data structures");

    // Allocate space for driver state data structure
    BSG_PCI_DEV = kzalloc(sizeof(struct bsg_pci_device), GFP_KERNEL);
    if (!BSG_PCI_DEV) {
        rc = -ENOMEM;

        BSG_DEBUG("Error calling malloc for bsg_pci_device struct in bbd_alloc");
        BSG_DEBUG("Exiting bbd_alloc in error state: rc=%d", rc);

        return rc;
    }

    // Store some basic device information
    BSG_PCI_DEV->pcidev = pcidev;
    BSG_PCI_DEV->dev = &pcidev->dev;
    
    // Store pointer to driver state in device
    dev_set_drvdata(BSG_PCI_DEV->dev, BSG_PCI_DEV);

    // Call bbd_setup routine
    rc = bsg_pci_setup(BSG_PCI_DEV);
    if (rc != 0) {
        BSG_DEBUG("Error in bbd_setup called from bbd_alloc");

        kfree(BSG_PCI_DEV);

        return rc;
    }
    
    return 0;
}

int bsg_pci_setup(struct bsg_pci_device *BSG_PCI_DEV)
{
    BSG_PCI_DEV->driver_version = 1;

    // Initialize hardware
    if (bsg_pci_hw_setup(BSG_PCI_DEV))
        return -EIO;

    // Initialize statistics
    bsg_pci_char_setup(BSG_PCI_DEV);

    BSG_DEBUG("Device Ready for Use");

    return 0;
}

//-----------------------------
// PCI-PROBE FUNCTION ENDS HERE
//-----------------------------

//--------------------------------
// PCI-REMOVE FUNCTION STARTS HERE
//--------------------------------

// Called on device removal/module unload
void __devexit bsg_pci_remove(struct pci_dev *dev)
{
    bsg_pci_free(dev);
}

int bsg_pci_free(struct pci_dev *pcidev)
{
    struct bsg_pci_device *BSG_PCI_DEV = dev_get_drvdata(&pcidev->dev);

    if (BSG_PCI_DEV) {
        bsg_pci_teardown(BSG_PCI_DEV);
        dev_set_drvdata(&pcidev->dev, NULL); 
        kfree(BSG_PCI_DEV);
    }

    return 0;
}

void bsg_pci_teardown(struct bsg_pci_device *BSG_PCI_DEV)
{
    bsg_pci_char_teardown(BSG_PCI_DEV);
    bsg_pci_hw_teardown(BSG_PCI_DEV);
}

//------------------------------
// PCI-REMOVE FUNCTION ENDS HERE
//------------------------------

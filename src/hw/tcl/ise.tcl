#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: ise.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#          Qiaoshi Zheng - q5zheng@eng.ucsd.edu
#------------------------------------------------------------

set bsg_work_dir $::env(BSG_WORK_DIR)
set bsg_top_name $::env(BSG_TOP_NAME)

set src_dir $bsg_work_dir/src/hw
set ise_output_dir $bsg_work_dir/out/ise

set project_dir $ise_output_dir/$bsg_top_name
set proj_exts [ list ise xise gise ]

foreach ext $proj_exts {
  set proj_name "${project_dir}.$ext"
  if { [ file exists $proj_name ] } {
    file delete $proj_name
  }
}

project new $project_dir

project set family "Virtex6"
project set device "xc6vlx240t"
project set package "ff1156"
project set speed "-1"
project set top_level_module_type "HDL"
project set synthesis_tool "XST (VHDL/Verilog)"
project set simulator "ISim (VHDL/Verilog)"
project set "Preferred Language" "Verilog"
project set "Enable Message Filtering" "false"

xfile add $src_dir/v/bsg_fifo_async/bsg_fifo_async_mem.v
xfile add $src_dir/v/bsg_fifo_async/bsg_fifo_async_read.v
xfile add $src_dir/v/bsg_fifo_async/bsg_fifo_async_write.v
xfile add $src_dir/v/bsg_fifo_async/bsg_fifo_async.v
xfile add $src_dir/v/bsg_misc/bsg_pll_v6.v
xfile add $src_dir/v/bsg_misc/bsg_fifo.v
xfile add $src_dir/v/bsg_misc/bsg_size_fns.v
xfile add $src_dir/v/bsg_misc/bsg_credit_tracker.v
xfile add $src_dir/v/bsg_pcie/gtx_drp_chanalign_fix_3752_v6.v
xfile add $src_dir/v/bsg_pcie/gtx_rx_valid_filter_v6.v
xfile add $src_dir/v/bsg_pcie/gtx_tx_sync_rate_v6.v
xfile add $src_dir/v/bsg_pcie/gtx_wrapper_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_2_0_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_app_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_brams_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_bram_top_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_bram_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_clocking_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_gtx_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_pipe_lane_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_pipe_misc_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_pipe_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_reset_delay_v6.v
xfile add $src_dir/v/bsg_pcie/pcie_upconfig_fix_3451_v6.v
xfile add $src_dir/v/bsg_pcie/PIO_64_RX_ENGINE.v
xfile add $src_dir/v/bsg_pcie/PIO_64_TX_ENGINE.v
xfile add $src_dir/v/bsg_pcie/PIO_64.v
xfile add $src_dir/v/bsg_pcie/PIO_EP.v
xfile add $src_dir/v/bsg_pcie/PIO_TO_CTRL.v
xfile add $src_dir/v/bsg_pcie/PIO.v
xfile add $src_dir/v/bsg_pcie/v6_pcie_v1_7.v
xfile add $src_dir/v/bsg_pcie/bsg_pcie_fifo_async.v
xfile add $src_dir/v/bsg_pcie/pcie_pio.v
xfile add $src_dir/v/bsg_pcie/bsg_pcie.v
xfile add $src_dir/v/$bsg_top_name.v
xfile add $src_dir/ucf/$bsg_top_name.ucf

project set top "$bsg_top_name"

process run "Generate Programming File"

//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: pcie_fifo_async.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//          Qiaoshi Zheng - q5zheng@eng.ucsd.edu
//------------------------------------------------------------

module bsg_pcie_fifo_async #
  (parameter NUM_MUX_IO_PORTS_P = -1)
  // FPGA 
  (input clk_i
  ,input reset_i
  ,input [32*NUM_MUX_IO_PORTS_P - 1 : 0] data_i
  ,input [NUM_MUX_IO_PORTS_P - 1 : 0] valid_i
  ,output [NUM_MUX_IO_PORTS_P - 1 : 0] thanks_o
  ,output [32*NUM_MUX_IO_PORTS_P - 1 : 0] data_o
  ,output [NUM_MUX_IO_PORTS_P - 1 : 0] valid_o
  ,input [NUM_MUX_IO_PORTS_P - 1 : 0] yummy_i
  // PCIE
  ,input pcie_reset_i
  ,input pcie_clk_i
  ,input [32*NUM_MUX_IO_PORTS_P - 1 : 0] pcie_data_i
  ,output [NUM_MUX_IO_PORTS_P - 1 : 0] pcie_full_o
  ,output [32*NUM_MUX_IO_PORTS_P - 1 : 0] pcie_data_o
  ,output [NUM_MUX_IO_PORTS_P - 1 : 0] pcie_empty_o
  ,input [NUM_MUX_IO_PORTS_P - 1 : 0] p_receive_fifo_empty_i
  ,input [NUM_MUX_IO_PORTS_P - 1 : 0] p_transmit_fifo_full_i);

  // PCIE-TO-FPGA
  wire [NUM_MUX_IO_PORTS_P - 1 : 0] pcie_w_enq_lo, pcie_w_full_lo;
  wire [NUM_MUX_IO_PORTS_P - 1 : 0] r_empty_lo, r_valid_lo;
  wire [NUM_MUX_IO_PORTS_P - 1 : 0] ct_avail_lo;

  genvar i;
  generate
    for (i = 0; i < NUM_MUX_IO_PORTS_P; i = i + 1) begin: pcie_to_fpga
      
      assign pcie_w_enq_lo[i] = (~pcie_w_full_lo[i]) & (~p_receive_fifo_empty_i[i]);
       
      bsg_fifo_async #
        (.width_p(32)
        ,.length_width_p(3))
      bsg_fa_p2f_inst
        (.w_clk_i(pcie_clk_i)
        ,.w_reset_i(pcie_reset_i)
        ,.w_enq_i(pcie_w_enq_lo[i])
        ,.w_data_i(pcie_data_i[32*i + 31 : 32*i])
        ,.w_full_o(pcie_w_full_lo[i])
        ,.r_clk_i(clk_i)
        ,.r_reset_i(reset_i)
        ,.r_data_o(data_o[32*i + 31 : 32*i])
        ,.r_deq_i(r_valid_lo[i])
        ,.r_empty_o(r_empty_lo[i])); 

      bsg_credit_tracker #
        (.num_credits_p(4))
      bsg_ct_inst
        (.clk_i(clk_i)
        ,.reset_i(reset_i)
        ,.yummy_i(yummy_i[i])
        ,.valid_i(r_valid_lo[i])
        ,.avail_o(ct_avail_lo[i]));

      assign r_valid_lo[i] = (~r_empty_lo[i]) & ct_avail_lo[i];

    end
  endgenerate

  assign valid_o = r_valid_lo;
  assign pcie_full_o = pcie_w_full_lo;
 
  // FPGA-TO-PCIE
  wire [NUM_MUX_IO_PORTS_P - 1 : 0] fifo_empty_lo;
  wire [32 * NUM_MUX_IO_PORTS_P - 1 : 0] fifo_data_lo;

  wire [NUM_MUX_IO_PORTS_P - 1 : 0] thanks_lo;
  wire [NUM_MUX_IO_PORTS_P - 1 : 0] w_full_lo; 
  wire [NUM_MUX_IO_PORTS_P - 1 : 0] pcie_r_empty_lo, pcie_r_deq_lo;

  generate
    for (i = 0; i < NUM_MUX_IO_PORTS_P; i = i + 1) begin: fpga_to_pcie
     
      bsg_fifo #
        (.I_WIDTH(32)
	,.A_WIDTH(0)
	,.LG_DEPTH(3))
      bsg_f_f2p_inst
        (.clk(clk_i)
        ,.din(data_i[32*i + 31 : 32*i])
        ,.enque(valid_i[i])
        ,.deque(thanks_lo[i])
        ,.clear(reset_i)
        ,.dout(fifo_data_lo[32*i + 31 : 32*i])
        ,.empty(fifo_empty_lo[i])
        ,.full()
        ,.almost_full()
        ,.valid());
      
      bsg_fifo_async #
        (.width_p(32)
        ,.length_width_p(3))
      bsg_fa_f2p_inst
        (.w_clk_i(clk_i)
        ,.w_reset_i(reset_i)
        ,.w_enq_i(thanks_lo[i])
        ,.w_data_i(fifo_data_lo[32*i + 31 : 32*i])
        ,.w_full_o(w_full_lo[i])
        ,.r_clk_i(pcie_clk_i)
        ,.r_reset_i(pcie_reset_i)
        ,.r_data_o(pcie_data_o[32*i + 31 : 32*i])
        ,.r_deq_i(pcie_r_deq_lo)
        ,.r_empty_o(pcie_r_empty_lo[i]));

      assign thanks_lo[i] = (~fifo_empty_lo[i]) & (~w_full_lo[i]);
      assign pcie_r_deq_lo[i] = (~pcie_r_empty_lo[i]) & (~p_transmit_fifo_full_i[i]);

    end
  endgenerate

  assign pcie_empty_o = pcie_r_empty_lo;
  assign thanks_o = thanks_lo;

endmodule

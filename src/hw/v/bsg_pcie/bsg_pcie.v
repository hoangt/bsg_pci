//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: pcie_top.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//          Qiaoshi Zheng - q5zheng@eng.ucsd.edu
//------------------------------------------------------------

`timescale 1ns / 1ps

module bsg_pcie #
  (parameter PL_FAST_TRAIN = "FALSE"
  ,parameter NUM_MUX_IO_PORTS_P = 2)
  (input clk_i
  ,input reset_i
  // For data towards the linux box
  ,input [32*NUM_MUX_IO_PORTS_P - 1:0] data_i
  ,input [NUM_MUX_IO_PORTS_P - 1:0] valid_i
  ,output [NUM_MUX_IO_PORTS_P - 1:0] thanks_o
  // For data into the fpga
  ,output [32*NUM_MUX_IO_PORTS_P - 1:0] data_o
  ,output [NUM_MUX_IO_PORTS_P - 1:0] valid_o
  ,input  [NUM_MUX_IO_PORTS_P - 1:0] yummy_i
  // PCIE
  ,input PCIE_CLK_P, PCIE_CLK_N
  ,input PCIE_PRSNT_B_FPGA
  ,output [3:0] PCIE_TX_P, PCIE_TX_N
  ,input [3:0] PCIE_RX_P, PCIE_RX_N
  // MISC
  ,input  [31:0] pcie_status_reg_i
  ,output host_reset_o
  ,output [31:0] debug_o);

  // 100MHz from PCI-E Host Computer
  wire pcie_clk_lo;
  IBUFDS_GTXE1 ibufds_pcie_clk
    (.I(PCIE_CLK_P) ,.IB(PCIE_CLK_N)
    ,.O(pcie_clk_lo));

  // PCI-E  RESET_N
  wire pcie_reset_n_lo;
  IBUF ibuf_pcie_reset
    (.I(PCIE_PRSNT_B_FPGA)
    ,.O(pcie_reset_n_lo));

  wire trn_clk;
  wire trn_reset_n;
  wire trn_lnk_up_n;
  // TX
  wire [5:0] trn_tbuf_av;

  wire trn_tcfg_req_n;
  wire trn_terr_drop_n;
  wire trn_tdst_rdy_n;

  wire [63:0] trn_td;

  wire trn_trem_n;
  wire trn_tsof_n;
  wire trn_teof_n;
  wire trn_tsrc_rdy_n;
  wire trn_tsrc_dsc_n;
  wire trn_terrfwd_n;
  wire trn_tcfg_gnt_n;
  wire trn_tstr_n;

  // RX
  wire [63:0] trn_rd;

  wire trn_rrem_n;
  wire trn_rsof_n;
  wire trn_reof_n;
  wire trn_rsrc_rdy_n;
  wire trn_rsrc_dsc_n;
  wire trn_rerrfwd_n;

  wire [6:0] trn_rbar_hit_n;
  wire trn_rdst_rdy_n;
  wire trn_rnp_ok_n;

  // Flow Control
  wire [11:0] trn_fc_cpld;
  wire [7:0] trn_fc_cplh;
  wire [11:0] trn_fc_npd;
  wire [7:0] trn_fc_nph;
  wire [11:0] trn_fc_pd;
  wire [7:0] trn_fc_ph;
  wire [2:0] trn_fc_sel;

  //-------------------------------------------------------
  // 3. Configuration (CFG) Interface
  //-------------------------------------------------------

  wire [31:0] cfg_do;

  wire cfg_rd_wr_done_n;

  wire [31:0] cfg_di;
  wire [3:0] cfg_byte_en_n;
  wire [9:0] cfg_dwaddr;

  wire cfg_wr_en_n;
  wire cfg_rd_en_n;
  wire cfg_err_cor_n;
  wire cfg_err_ur_n;
  wire cfg_err_ecrc_n;
  wire cfg_err_cpl_timeout_n;
  wire cfg_err_cpl_abort_n;
  wire cfg_err_cpl_unexpect_n;
  wire cfg_err_posted_n;
  wire cfg_err_locked_n;

  wire [47:0] cfg_err_tlp_cpl_header;

  wire cfg_err_cpl_rdy_n;
  wire cfg_interrupt_n;
  wire cfg_interrupt_rdy_n;
  wire cfg_interrupt_assert_n;

  wire [7:0] cfg_interrupt_di;
  wire [7:0] cfg_interrupt_do;
  wire [2:0] cfg_interrupt_mmenable;

  wire cfg_interrupt_msienable;
  wire cfg_interrupt_msixenable;
  wire cfg_interrupt_msixfm;
  wire cfg_turnoff_ok_n;
  wire cfg_to_turnoff_n;
  wire cfg_trn_pending_n;
  wire cfg_pm_wake_n;

  wire [7:0] cfg_bus_number;
  wire [4:0] cfg_device_number;
  wire [2:0] cfg_function_number;

  wire [15:0] cfg_status;
  wire [15:0] cfg_command;
  wire [15:0] cfg_dstatus;
  wire [15:0] cfg_dcommand;
  wire [15:0] cfg_lstatus;
  wire [15:0] cfg_lcommand;
  wire [15:0] cfg_dcommand2;

  wire [2:0] cfg_pcie_link_state_n;
  wire [63:0] cfg_dsn;

  //-------------------------------------------------------
  // 4. Physical Layer Control and Status (PL) Interface
  //-------------------------------------------------------

  wire [2:0] pl_initial_link_width;
  wire [1:0] pl_lane_reversal_mode;

  wire pl_link_gen2_capable;
  wire pl_link_partner_gen2_supported;
  wire pl_link_upcfg_capable;

  wire [5:0] pl_ltssm_state;

  wire pl_received_hot_rst;
  wire pl_sel_link_rate;

  wire [1:0] pl_sel_link_width;

  wire pl_directed_link_auton;

  wire [1:0] pl_directed_link_change;

  wire pl_directed_link_speed;

  wire [1:0] pl_directed_link_width;

  wire pl_upstream_prefer_deemph;

  //-------------------------------------------------------

  // For data towards the linux box (in pcie clock domain)
  wire [32*NUM_MUX_IO_PORTS_P - 1:0] data_FPGA_to_Linux_pci;
  wire [NUM_MUX_IO_PORTS_P - 1:0] valid_FPGA_to_Linux_pci;
  wire [NUM_MUX_IO_PORTS_P - 1:0] thanks_FPGA_to_Linux_pci;

  // For data into the fpga (in pcie clock domain)
  wire [32*NUM_MUX_IO_PORTS_P - 1:0] data_Linux_to_FPGA_pci;
  wire [NUM_MUX_IO_PORTS_P - 1:0] valid_Linux_to_FPGA_pci;
  wire [NUM_MUX_IO_PORTS_P - 1:0] yummy_Linux_to_FPGA_pci;

  // Status of the Asfifo
  wire [NUM_MUX_IO_PORTS_P - 1:0] pcie_bpd_asfifo_wfull;
  wire [NUM_MUX_IO_PORTS_P - 1:0] pcie_bpd_asfifo_rempty;
  wire [NUM_MUX_IO_PORTS_P - 1:0] bpd_pcie_asfifo_wfull;
  wire [NUM_MUX_IO_PORTS_P - 1:0] bpd_pcie_asfifo_rempty;
  wire [NUM_MUX_IO_PORTS_P - 1:0] p_receive_fifo_empty;
  wire [NUM_MUX_IO_PORTS_P - 1:0] p_transmit_fifo_full;

  wire host_commanding_reset_pcie;
  wire host_commanding_reset_raw_wire;

  reg host_commanding_reset_raw_reg1;
  reg host_commanding_reset_raw_reg2;

  // begin:  Reset synchronizer
  always @(posedge clk_i) begin
    if (host_commanding_reset_raw_wire)
      {host_commanding_reset_raw_reg2, host_commanding_reset_raw_reg1} <= {host_commanding_reset_raw_reg1, 1'b1};
    else
      {host_commanding_reset_raw_reg2, host_commanding_reset_raw_reg1} <= {host_commanding_reset_raw_reg1, 1'b0};
  end

  assign host_reset_o = host_commanding_reset_raw_reg2;
  // end:  Reset synchronizer

  bsg_pcie_fifo_async #
    (.NUM_MUX_IO_PORTS_P(NUM_MUX_IO_PORTS_P))
  bsg_pfa_inst
    // FPGA
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.data_i(data_i)
    ,.valid_i(valid_i)
    ,.thanks_o(thanks_o)
    ,.data_o(data_o)
    ,.valid_o(valid_o)
    ,.yummy_i(yummy_i)
    //  PCI-E clock domains
    ,.pcie_reset_i(pcie_reset_n_lo & (~host_commanding_reset_pcie))
    ,.pcie_clk_i(trn_clk)
    ,.pcie_data_i(data_Linux_to_FPGA_pci)
    ,.pcie_full_o(pcie_bpd_asfifo_wfull)
    ,.pcie_data_o(data_FPGA_to_Linux_pci)
    ,.pcie_empty_o(bpd_pcie_asfifo_rempty)
    ,.p_receive_fifo_empty_i(p_receive_fifo_empty)
    ,.p_transmit_fifo_full_i(p_transmit_fifo_full));

  FDCP #
    (.INIT(1'b1))
  trn_lnk_up_n_int_i
    (.Q (trn_lnk_up_n)
    ,.D (trn_lnk_up_n_int1)
    ,.C (trn_clk)
    ,.CLR (1'b0)
    ,.PRE (1'b0));

  FDCP #
    (.INIT(1'b1))
  trn_reset_n_i
    (.Q (trn_reset_n)
    ,.D (trn_reset_n_int1)
    ,.C (trn_clk)
    ,.CLR (1'b0)
    ,.PRE (1'b0));


`ifdef SIMULATION
  v6_pcie_v1_7 #
    (.PL_FAST_TRAIN(PL_FAST_TRAIN))
  core
`else
  v6_pcie_v1_7 core
`endif

  //-------------------------------------------------------
  // 1. PCI Express (pci_exp) Interface
  //-------------------------------------------------------

  // TX
  (.pci_exp_txp(PCIE_TX_P)
  ,.pci_exp_txn(PCIE_TX_N)

  // RX
  ,.pci_exp_rxp(PCIE_RX_P)
  ,.pci_exp_rxn(PCIE_RX_N)

  //-------------------------------------------------------
  // 2. Transaction (TRN) Interface
  //-------------------------------------------------------

  // COMMON
  ,.trn_clk(trn_clk)
  ,.trn_reset_n(trn_reset_n_int1)
  ,.trn_lnk_up_n(trn_lnk_up_n_int1)

  // TX
  ,.trn_tbuf_av(trn_tbuf_av)
  ,.trn_tcfg_req_n(trn_tcfg_req_n)
  ,.trn_terr_drop_n(trn_terr_drop_n)
  ,.trn_tdst_rdy_n(trn_tdst_rdy_n)
  ,.trn_td(trn_td)
  ,.trn_trem_n(trn_trem_n)
  ,.trn_tsof_n(trn_tsof_n)
  ,.trn_teof_n(trn_teof_n)
  ,.trn_tsrc_rdy_n(trn_tsrc_rdy_n)
  ,.trn_tsrc_dsc_n(trn_tsrc_dsc_n)
  ,.trn_terrfwd_n(trn_terrfwd_n)
  ,.trn_tcfg_gnt_n(trn_tcfg_gnt_n)
  ,.trn_tstr_n(trn_tstr_n)

  // RX
  ,.trn_rd(trn_rd)
  ,.trn_rrem_n(trn_rrem_n)
  ,.trn_rsof_n(trn_rsof_n)
  ,.trn_reof_n(trn_reof_n)
  ,.trn_rsrc_rdy_n(trn_rsrc_rdy_n)
  ,.trn_rsrc_dsc_n(trn_rsrc_dsc_n)
  ,.trn_rerrfwd_n(trn_rerrfwd_n)
  ,.trn_rbar_hit_n(trn_rbar_hit_n)
  ,.trn_rdst_rdy_n(trn_rdst_rdy_n)
  ,.trn_rnp_ok_n(trn_rnp_ok_n)

  // FLOW CONTROL
  ,.trn_fc_cpld(trn_fc_cpld)
  ,.trn_fc_cplh(trn_fc_cplh)
  ,.trn_fc_npd(trn_fc_npd)
  ,.trn_fc_nph(trn_fc_nph)
  ,.trn_fc_pd(trn_fc_pd)
  ,.trn_fc_ph(trn_fc_ph)
  ,.trn_fc_sel(trn_fc_sel)

  //-------------------------------------------------------
  // 3. Configuration (CFG) Interface
  //-------------------------------------------------------

  ,.cfg_do(cfg_do)
  ,.cfg_rd_wr_done_n(cfg_rd_wr_done_n)
  ,.cfg_di(cfg_di)
  ,.cfg_byte_en_n(cfg_byte_en_n)
  ,.cfg_dwaddr(cfg_dwaddr)
  ,.cfg_wr_en_n(cfg_wr_en_n)
  ,.cfg_rd_en_n(cfg_rd_en_n)
  ,.cfg_err_cor_n(cfg_err_cor_n)
  ,.cfg_err_ur_n(cfg_err_ur_n)
  ,.cfg_err_ecrc_n(cfg_err_ecrc_n)
  ,.cfg_err_cpl_timeout_n(cfg_err_cpl_timeout_n)
  ,.cfg_err_cpl_abort_n(cfg_err_cpl_abort_n)
  ,.cfg_err_cpl_unexpect_n(cfg_err_cpl_unexpect_n)
  ,.cfg_err_posted_n(cfg_err_posted_n)
  ,.cfg_err_locked_n(cfg_err_locked_n)
  ,.cfg_err_tlp_cpl_header(cfg_err_tlp_cpl_header)
  ,.cfg_err_cpl_rdy_n(cfg_err_cpl_rdy_n)
  ,.cfg_interrupt_n(cfg_interrupt_n)
  ,.cfg_interrupt_rdy_n(cfg_interrupt_rdy_n)
  ,.cfg_interrupt_assert_n(cfg_interrupt_assert_n)
  ,.cfg_interrupt_di(cfg_interrupt_di)
  ,.cfg_interrupt_do(cfg_interrupt_do)
  ,.cfg_interrupt_mmenable(cfg_interrupt_mmenable)
  ,.cfg_interrupt_msienable(cfg_interrupt_msienable)
  ,.cfg_interrupt_msixenable(cfg_interrupt_msixenable)
  ,.cfg_interrupt_msixfm(cfg_interrupt_msixfm)
  ,.cfg_turnoff_ok_n(cfg_turnoff_ok_n)
  ,.cfg_to_turnoff_n(cfg_to_turnoff_n)
  ,.cfg_trn_pending_n(cfg_trn_pending_n)
  ,.cfg_pm_wake_n(cfg_pm_wake_n)
  ,.cfg_bus_number(cfg_bus_number)
  ,.cfg_device_number(cfg_device_number)
  ,.cfg_function_number(cfg_function_number)
  ,.cfg_status(cfg_status)
  ,.cfg_command(cfg_command)
  ,.cfg_dstatus(cfg_dstatus)
  ,.cfg_dcommand(cfg_dcommand)
  ,.cfg_lstatus(cfg_lstatus)
  ,.cfg_lcommand(cfg_lcommand)
  ,.cfg_dcommand2(cfg_dcommand2)
  ,.cfg_pcie_link_state_n(cfg_pcie_link_state_n)
  ,.cfg_dsn(cfg_dsn)
  ,.cfg_pmcsr_pme_en()
  ,.cfg_pmcsr_pme_status()
  ,.cfg_pmcsr_powerstate()

  //-------------------------------------------------------
  // 4. Physical Layer Control and Status (PL) Interface
  //-------------------------------------------------------

  ,.pl_initial_link_width(pl_initial_link_width)
  ,.pl_lane_reversal_mode(pl_lane_reversal_mode)
  ,.pl_link_gen2_capable(pl_link_gen2_capable)
  ,.pl_link_partner_gen2_supported(pl_link_partner_gen2_supported)
  ,.pl_link_upcfg_capable(pl_link_upcfg_capable)
  ,.pl_ltssm_state(pl_ltssm_state)
  ,.pl_received_hot_rst(pl_received_hot_rst)
  ,.pl_sel_link_rate(pl_sel_link_rate)
  ,.pl_sel_link_width(pl_sel_link_width)
  ,.pl_directed_link_auton(pl_directed_link_auton)
  ,.pl_directed_link_change(pl_directed_link_change)
  ,.pl_directed_link_speed(pl_directed_link_speed)
  ,.pl_directed_link_width(pl_directed_link_width)
  ,.pl_upstream_prefer_deemph(pl_upstream_prefer_deemph)

  //-------------------------------------------------------
  // 5. System  (SYS) Interface
  //-------------------------------------------------------

  ,.sys_clk(pcie_clk_lo)
  ,.sys_reset_n(pcie_reset_n_lo));


  pcie_app_v6 #
    (.NUM_MUX_IO_PORTS_P(NUM_MUX_IO_PORTS_P))
  app

  //-------------------------------------------------------
  // 1. Transaction (TRN) Interface
  //-------------------------------------------------------

  // COMMON
  (.trn_clk(trn_clk)
  ,.trn_reset_n(trn_reset_n_int1)
  ,.trn_lnk_up_n(trn_lnk_up_n_int1)

  // TX
  ,.trn_tbuf_av(trn_tbuf_av)
  ,.trn_tcfg_req_n(trn_tcfg_req_n)
  ,.trn_terr_drop_n(trn_terr_drop_n)
  ,.trn_tdst_rdy_n(trn_tdst_rdy_n)
  ,.trn_td(trn_td)
  ,.trn_trem_n(trn_trem_n)
  ,.trn_tsof_n(trn_tsof_n)
  ,.trn_teof_n(trn_teof_n)
  ,.trn_tsrc_rdy_n(trn_tsrc_rdy_n)
  ,.trn_tsrc_dsc_n(trn_tsrc_dsc_n)
  ,.trn_terrfwd_n(trn_terrfwd_n)
  ,.trn_tcfg_gnt_n(trn_tcfg_gnt_n)
  ,.trn_tstr_n(trn_tstr_n)

  // RX
  ,.trn_rd(trn_rd)
  ,.trn_rrem_n(trn_rrem_n)
  ,.trn_rsof_n(trn_rsof_n)
  ,.trn_reof_n(trn_reof_n)
  ,.trn_rsrc_rdy_n(trn_rsrc_rdy_n)
  ,.trn_rsrc_dsc_n(trn_rsrc_dsc_n)
  ,.trn_rerrfwd_n(trn_rerrfwd_n)
  ,.trn_rbar_hit_n(trn_rbar_hit_n)
  ,.trn_rdst_rdy_n(trn_rdst_rdy_n)
  ,.trn_rnp_ok_n(trn_rnp_ok_n)

  // FLOW CONTROL
  ,.trn_fc_cpld(trn_fc_cpld)
  ,.trn_fc_cplh(trn_fc_cplh)
  ,.trn_fc_npd(trn_fc_npd)
  ,.trn_fc_nph(trn_fc_nph)
  ,.trn_fc_pd(trn_fc_pd)
  ,.trn_fc_ph(trn_fc_ph)
  ,.trn_fc_sel(trn_fc_sel)

  //-------------------------------------------------------
  // 2. Configuration (CFG) Interface
  //-------------------------------------------------------

  ,.cfg_do(cfg_do)
  ,.cfg_rd_wr_done_n(cfg_rd_wr_done_n)
  ,.cfg_di(cfg_di)
  ,.cfg_byte_en_n(cfg_byte_en_n)
  ,.cfg_dwaddr(cfg_dwaddr)
  ,.cfg_wr_en_n(cfg_wr_en_n)
  ,.cfg_rd_en_n(cfg_rd_en_n)
  ,.cfg_err_cor_n(cfg_err_cor_n)
  ,.cfg_err_ur_n(cfg_err_ur_n)
  ,.cfg_err_ecrc_n(cfg_err_ecrc_n)
  ,.cfg_err_cpl_timeout_n(cfg_err_cpl_timeout_n)
  ,.cfg_err_cpl_abort_n(cfg_err_cpl_abort_n)
  ,.cfg_err_cpl_unexpect_n(cfg_err_cpl_unexpect_n)
  ,.cfg_err_posted_n(cfg_err_posted_n)
  ,.cfg_err_locked_n(cfg_err_locked_n)
  ,.cfg_err_tlp_cpl_header(cfg_err_tlp_cpl_header)
  ,.cfg_err_cpl_rdy_n(cfg_err_cpl_rdy_n)
  ,.cfg_interrupt_n(cfg_interrupt_n)
  ,.cfg_interrupt_rdy_n(cfg_interrupt_rdy_n)
  ,.cfg_interrupt_assert_n(cfg_interrupt_assert_n)
  ,.cfg_interrupt_di(cfg_interrupt_di)
  ,.cfg_interrupt_do(cfg_interrupt_do)
  ,.cfg_interrupt_mmenable(cfg_interrupt_mmenable)
  ,.cfg_interrupt_msienable(cfg_interrupt_msienable)
  ,.cfg_interrupt_msixenable(cfg_interrupt_msixenable)
  ,.cfg_interrupt_msixfm(cfg_interrupt_msixfm)
  ,.cfg_turnoff_ok_n(cfg_turnoff_ok_n)
  ,.cfg_to_turnoff_n(cfg_to_turnoff_n)
  ,.cfg_trn_pending_n(cfg_trn_pending_n)
  ,.cfg_pm_wake_n(cfg_pm_wake_n)
  ,.cfg_bus_number(cfg_bus_number)
  ,.cfg_device_number(cfg_device_number)
  ,.cfg_function_number(cfg_function_number)
  ,.cfg_status(cfg_status)
  ,.cfg_command(cfg_command)
  ,.cfg_dstatus(cfg_dstatus)
  ,.cfg_dcommand(cfg_dcommand)
  ,.cfg_lstatus(cfg_lstatus)
  ,.cfg_lcommand(cfg_lcommand)
  ,.cfg_dcommand2(cfg_dcommand2)
  ,.cfg_pcie_link_state_n(cfg_pcie_link_state_n)
  ,.cfg_dsn(cfg_dsn)

  //-------------------------------------------------------
  // 3. Physical Layer Control and Status (PL) Interface
  //-------------------------------------------------------

  ,.pl_initial_link_width(pl_initial_link_width)
  ,.pl_lane_reversal_mode(pl_lane_reversal_mode)
  ,.pl_link_gen2_capable(pl_link_gen2_capable)
  ,.pl_link_partner_gen2_supported(pl_link_partner_gen2_supported)
  ,.pl_link_upcfg_capable(pl_link_upcfg_capable)
  ,.pl_ltssm_state(pl_ltssm_state)
  ,.pl_received_hot_rst(pl_received_hot_rst)
  ,.pl_sel_link_rate(pl_sel_link_rate)
  ,.pl_sel_link_width(pl_sel_link_width)
  ,.pl_directed_link_auton(pl_directed_link_auton)
  ,.pl_directed_link_change(pl_directed_link_change)
  ,.pl_directed_link_speed(pl_directed_link_speed)
  ,.pl_directed_link_width(pl_directed_link_width)
  ,.pl_upstream_prefer_deemph(pl_upstream_prefer_deemph)
  // For data towards the linux box (in pcie clock domain)
  ,.data_FPGA_to_Linux_pci(data_FPGA_to_Linux_pci)
  // For data into the fpga (in pcie clock domain)
  ,.data_Linux_to_FPGA_pci(data_Linux_to_FPGA_pci)
  // Status of the Asfifo
  ,.pcie_bpd_asfifo_wfull(pcie_bpd_asfifo_wfull)
  ,.bpd_pcie_asfifo_rempty(bpd_pcie_asfifo_rempty)
  ,.p_receive_fifo_empty(p_receive_fifo_empty)
  ,.p_transmit_fifo_full(p_transmit_fifo_full)
  ,.host_command_reset(host_commanding_reset_pcie)
  ,.host_command_reset_raw(host_commanding_reset_raw_wire)
  ,.status_reg(pcie_status_reg_i)
  ,.bpd_test_reg(debug_o));

endmodule

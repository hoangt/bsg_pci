//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: pcie_pio.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//          Qiaoshi Zheng - q5zheng@eng.ucsd.edu
//------------------------------------------------------------

//--------------------------------------------------------------
//                PIO Addressing
//--------------------------------------------------------------
// Channels
//     addr_i (rd/wr)       addr_i[5:4]        addr_i[3:0]
//     receive_status         0 0                  i
//     receive_fifo           0 1                  i
//     transmit_fifo          1 0                  i
//     transmit_status        1 1                  i
//
// Other useful Regs
//     NAME                 Address(Linux)   Address(PIO)
//     bpd_channel_number    0x7fc            0x1ff (1_1111_1111)
//     bpd_host_reset        0x7f8            0x1fe (1_1111_1110)
//     bpd_test_reg          0x7f4            0x1fd (1_1111_1101)
//     bpd_status_reg        0x7f0            0x1fc (1_1111_1100)
//--------------------------------------------------------------

module pcie_pio #
  (parameter NUM_MUX_IO_PORTS_P = 2)
  (input pci_exp_reset
  ,input pci_exp_clk
  ,input [32*NUM_MUX_IO_PORTS_P - 1:0] data_FPGA_to_Linux_pci
  ,output [32*NUM_MUX_IO_PORTS_P - 1:0] data_Linux_to_FPGA_pci
  // status of the Asfifo
  ,input [NUM_MUX_IO_PORTS_P - 1:0] pcie_bpd_asfifo_wfull
  ,input [NUM_MUX_IO_PORTS_P - 1:0] bpd_pcie_asfifo_rempty
  // Interface to the pcie_fifo_async module
  ,output  [NUM_MUX_IO_PORTS_P - 1:0] p_receive_fifo_empty
  ,output  [NUM_MUX_IO_PORTS_P - 1:0] p_transmit_fifo_full
  // Interface to the PCI-FSM
  // Read Port
  ,input [10:0] rd_addr_i
  ,input [3:0] rd_be_i
  ,output [31:0] rd_data_o
  // Write Port
  ,input [10:0] wr_addr_i
  ,input [7:0] wr_be_i
  ,input [31:0] wr_data_i
  ,input wr_en_i
  ,output wr_busy_o
  ,output reg host_command_reset
  ,output reg host_command_reset_raw   // slow clock domain
  ,input [31:0] status_reg
  ,output reg [31:0] bpd_test_reg);

  localparam FPGA_BUFFER_DEPTH_LG = 9;
  localparam PCIE_RESET_CYCLES = 550;
  localparam RESET_CYCLES = 500;
  localparam RESET_IDLE = 3'b001;
  localparam RAW_RESET  = 3'b010;
  localparam PCIE_RESET = 3'b100;

  reg [2:0] state_r, state_n;

  reg [31:0] p_receive_fifo_status [0:NUM_MUX_IO_PORTS_P - 1];
  reg [31:0] p_transmit_fifo_status [0:NUM_MUX_IO_PORTS_P - 1];

  reg [NUM_MUX_IO_PORTS_P - 1:0] p_receive_fifo_enque;

  wire [NUM_MUX_IO_PORTS_P - 1:0] p_receive_fifo_deque;
  wire [NUM_MUX_IO_PORTS_P - 1:0] p_receive_fifo_full;

  reg [32*NUM_MUX_IO_PORTS_P - 1:0] p_receive_fifo_in;

  reg [NUM_MUX_IO_PORTS_P - 1:0] p_transmit_fifo_deque;
  wire [NUM_MUX_IO_PORTS_P - 1:0] p_transmit_fifo_empty;

  wire [32*16:0] p_transmit_fifo_out;

  assign p_transmit_fifo_out[32 * 16 : (32 * NUM_MUX_IO_PORTS_P)] = { (32 * (16-NUM_MUX_IO_PORTS_P) + 1) {1'b0}};

  reg [31:0] rd_data_o_reg;
  reg [31:0] rd_data_o_wire;
  reg [31:0] rd_data_o_wire_0;
  reg [31:0] rd_data_o_wire_1;
  reg [31:0] rd_data_o_wire_2;
  reg [31:0] rd_data_o_wire_3;

  reg [15:0] host_command_reset_reg;
  reg [9:0] reset_counter;

  wire [3:0] read_index;

  assign read_index = rd_addr_i[3:0];

  wire [3:0] write_index;

  assign write_index = wr_addr_i[3:0];

  // start: status_reg synchronizer
  reg [31:0] status_reg_1;
  reg [31:0] status_reg_2;

  always @(posedge pci_exp_clk)
    {status_reg_2, status_reg_1} <= {status_reg_1, status_reg};
  // end: status_reg synchronizer


  // always to generate the p_receive_fifo_enque
  always @( wr_addr_i or wr_en_i or write_index) begin
    p_receive_fifo_enque = 0;
    if (wr_addr_i[10:4] == 7'b0000_001 & wr_en_i == 1'b1)
      p_receive_fifo_enque[write_index] = 1'b1;
    else begin  // no latch
      p_receive_fifo_enque[write_index] = 1'b0;
    end
  end

  wire  host_command_reset_enable;
  assign host_command_reset_enable = wr_addr_i[8:0] == 9'b1_1111_1110 & wr_en_i == 1'b1 & wr_data_i == 32'hffff_ffff;

  // Circuit to generate the host command reset
  always @(posedge pci_exp_clk)
    if ( !pci_exp_reset)
      state_r <= RESET_IDLE;
    else
      state_r <= state_n;

  // Begin: Circuit to generate the host command reset
  always begin: set_next_state_outputs
    state_n = RESET_IDLE;
    host_command_reset = 1'b0;
    host_command_reset_raw = 1'b0;
    case (1'b1)
      state_r[0]: begin
        if (host_command_reset_enable) begin
          state_n = RAW_RESET;
          host_command_reset = 1'b1;
          host_command_reset_raw = 1'b1;
        end
        else begin
          state_n = RESET_IDLE;
        end
      end
      state_r[1]: begin
        if (reset_counter == RESET_CYCLES) begin
          state_n = PCIE_RESET;
          host_command_reset = 1'b1;
        end
        else begin
          state_n = RAW_RESET;
          host_command_reset = 1'b1;
          host_command_reset_raw = 1'b1;
        end
      end
      state_r[2]: begin
        if ( reset_counter == PCIE_RESET_CYCLES) begin
          state_n = RESET_IDLE;
        end
        else begin
          state_n = PCIE_RESET;
          host_command_reset = 1'b1;
        end
      end
    endcase
  end: set_next_state_outputs


  always @(posedge pci_exp_clk) begin
    if ( !pci_exp_reset)  reset_counter <= {10{1'b0}};
    else begin
      case (1'b1)
        state_n[0]:    reset_counter <= {10{1'b0}};
        state_n[1]:    reset_counter <= reset_counter + 1'b1;
        state_n[2]:    reset_counter <= reset_counter + 1'b1;
      endcase
    end
  end
  // End: Circuit to generate the host command reset

  // Circuit to generate the bpd_test_reg
  always @(posedge pci_exp_clk or negedge pci_exp_reset) begin
    if( !pci_exp_reset | host_command_reset) begin
      bpd_test_reg <= 32'hffff_ff_f0;
    end
    else begin
      if (wr_addr_i[8:0] == 9'b1_1111_1101 & wr_en_i == 1'b1) begin
        bpd_test_reg <= wr_data_i;
      end
    end
  end

  always @(rd_addr_i or read_index) begin
    p_transmit_fifo_deque = 0;
    if ( rd_addr_i[10:4] == 7'b0000_010) begin
      p_transmit_fifo_deque[read_index] = 1'b1;
    end
    else begin
      p_transmit_fifo_deque[read_index] = 1'b0;
    end
  end

  assign wr_busy_o = 1'b0;

  // In verilog, we need to use a 16 to 1 mux to code this circuit,
  // and it's better to use 2 level mux, which means use 5 4-1 mux.
  // Always to generate the rd_data_o_reg

  always @(posedge pci_exp_clk or negedge pci_exp_reset) begin
    if (!pci_exp_reset | host_command_reset)
      rd_data_o_reg <= 32'b0;
    else begin
      if (rd_addr_i[10:4] == 7'b0000_000) begin   // Read the receive_status register
        rd_data_o_reg <= p_receive_fifo_status[read_index];
      end
      else if (rd_addr_i[10:4] == 7'b0000_010) begin
        rd_data_o_reg <= rd_data_o_wire;
      end
      else if (rd_addr_i[10:4] == 7'b0000_011) begin
        rd_data_o_reg <= p_transmit_fifo_status[read_index];
      end
      // Read the channel number, address is 0x7fc
      else if (rd_addr_i[8:0] == 9'b1_1111_1111) begin
        rd_data_o_reg <= NUM_MUX_IO_PORTS_P;
      end
      else if (rd_addr_i[8:0] == 9'b1_1111_1101) begin
        rd_data_o_reg <= bpd_test_reg;
      end
      else if (rd_addr_i[8:0] == 9'b1_1111_1100) begin
        rd_data_o_reg <= status_reg_2;
      end
      else begin
        rd_data_o_reg <= 32'b0;
      end
    end
  end

  assign rd_data_o = rd_data_o_reg;

  // Need a 16 to 1 mux, which has two level 4 to 1 mux, totally use 5 4-1mux
  // The second level

  always @ (rd_addr_i or rd_data_o_wire_0 or rd_data_o_wire_1 or rd_data_o_wire_2 or rd_data_o_wire_3) begin
    case (rd_addr_i[3:2])
      2'b00: begin
        rd_data_o_wire = rd_data_o_wire_0;
      end
      2'b01: begin
        rd_data_o_wire = rd_data_o_wire_1;
      end
      2'b10: begin
        rd_data_o_wire = rd_data_o_wire_2;
      end
      2'b11: begin
        rd_data_o_wire = rd_data_o_wire_3;
      end
    endcase
  end

  // The first level
  always @ (rd_addr_i or p_transmit_fifo_out) begin
    case (rd_addr_i[1:0])
      2'b00: begin  //  0  4  8 12
        rd_data_o_wire_0 = p_transmit_fifo_out[ 32 * 0  + 31: 32 * 0 ];
        rd_data_o_wire_1 = p_transmit_fifo_out[ 32 * 4  + 31: 32 * 4 ];
        rd_data_o_wire_2 = p_transmit_fifo_out[ 32 * 8  + 31: 32 * 8 ];
        rd_data_o_wire_3 = p_transmit_fifo_out[ 32 * 12 + 31: 32 * 12 ];
      end
      2'b01: begin  //  1  5  9 13
        rd_data_o_wire_0 = p_transmit_fifo_out[ 32 * 1  + 31: 32 * 1 ];
        rd_data_o_wire_1 = p_transmit_fifo_out[ 32 * 5  + 31: 32 * 5 ];
        rd_data_o_wire_2 = p_transmit_fifo_out[ 32 * 9  + 31: 32 * 9 ];
        rd_data_o_wire_3 = p_transmit_fifo_out[ 32 * 13 + 31: 32 * 13];
      end
      2'b10: begin //   2  6 10 14
        rd_data_o_wire_0 = p_transmit_fifo_out[ 32 * 2  + 31: 32 * 2 ];
        rd_data_o_wire_1 = p_transmit_fifo_out[ 32 * 6  + 31: 32 * 6 ];
        rd_data_o_wire_2 = p_transmit_fifo_out[ 32 * 10 + 31: 32 * 10];
        rd_data_o_wire_3 = p_transmit_fifo_out[ 32 * 14 + 31: 32 * 14];
      end
      2'b11: begin //   3  7 11 15
        rd_data_o_wire_0 = p_transmit_fifo_out[ 32 * 3  + 31: 32 * 3 ];
        rd_data_o_wire_1 = p_transmit_fifo_out[ 32 * 7  + 31: 32 * 7 ];
        rd_data_o_wire_2 = p_transmit_fifo_out[ 32 * 11 + 31: 32 * 11];
        rd_data_o_wire_3 = p_transmit_fifo_out[ 32 * 15 + 31: 32 * 15];
      end
    endcase
  end

  genvar i;
  // PCI-E transmit fifo and status register
  generate
    for (i = 0; i < NUM_MUX_IO_PORTS_P ; i = i + 1) begin: pcie_transmit_fifo
      bsg_fifo #
        (.I_WIDTH(32)
	,.A_WIDTH(0)
	,.LG_DEPTH(FPGA_BUFFER_DEPTH_LG))
      p_transmit_fifo
        (.clk(pci_exp_clk)
        ,.din(data_FPGA_to_Linux_pci[i*32+31:i*32])
        ,.enque(!p_transmit_fifo_full[i] & !bpd_pcie_asfifo_rempty[i])
        ,.deque(p_transmit_fifo_deque[i])
        ,.clear(!pci_exp_reset | host_command_reset)
        ,.dout(p_transmit_fifo_out[i*32+31:i*32])
        ,.empty(p_transmit_fifo_empty[i])
        ,.full()
        ,.almost_full(p_transmit_fifo_full[i])
        ,.valid());

      // How many used entry in the transmit fifo
      always @(posedge pci_exp_clk or negedge pci_exp_reset) begin
        if(!pci_exp_reset | host_command_reset)
          p_transmit_fifo_status[i] <= 0;
        else
          p_transmit_fifo_status[i] <= p_transmit_fifo_status[i] - p_transmit_fifo_deque[i] + (!p_transmit_fifo_full[i] & !bpd_pcie_asfifo_rempty[i]);
      end
    end
  endgenerate

  // PCI-E receive fifo and status register
  generate
    for (i = 0; i < NUM_MUX_IO_PORTS_P ; i = i + 1) begin: pcie_receive_fifo
      bsg_fifo #
        (.I_WIDTH(32)
	,.A_WIDTH(0)
	,.LG_DEPTH(FPGA_BUFFER_DEPTH_LG))
      p_receive_fifo
        (.clk(pci_exp_clk)
        ,.din(wr_data_i)
        ,.enque(p_receive_fifo_enque[i])
        ,.deque(!pcie_bpd_asfifo_wfull[i] & !p_receive_fifo_empty[i])
        ,.clear(!pci_exp_reset | host_command_reset)
        ,.dout(data_Linux_to_FPGA_pci[i*32+31:i*32])
        ,.empty(p_receive_fifo_empty[i])
        ,.full()
        ,.almost_full(p_receive_fifo_full[i])
        ,.valid());

      // How many free entry in the receive fifo
      always @(posedge pci_exp_clk or negedge pci_exp_reset) begin
        if(!pci_exp_reset | host_command_reset)
          p_receive_fifo_status[i] <= (1 << FPGA_BUFFER_DEPTH_LG) - 2'b11;
        else
          p_receive_fifo_status[i] <= p_receive_fifo_status[i] - p_receive_fifo_enque[i] + (!pcie_bpd_asfifo_wfull[i] & !p_receive_fifo_empty[i]);
      end
    end
  endgenerate

endmodule

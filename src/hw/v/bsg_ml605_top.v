//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_ml605_top.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//          Qiaoshi Zheng - q5zheng@eng.ucsd.edu
//------------------------------------------------------------

module bsg_ml605_top
  (input SYSCLK_P, SYSCLK_N
  ,input FPGA_CPU_RESET_B
  ,input PCIE_CLK_P, PCIE_CLK_N
  ,input PCIE_PRSNT_B_FPGA
  ,output [3:0] PCIE_TX_P, PCIE_TX_N
  ,input [3:0] PCIE_RX_P, PCIE_RX_N
  ,output [7:0] GPIO_LED);

  // PLL
  wire pll_locked_lo;
  wire pll_50_mhz_lo;

  bsg_pll_v6 pll_v6_inst
    (.clk_p_i(SYSCLK_P) ,.clk_n_i(SYSCLK_N)
    ,.pll_locked_o(pll_locked_lo)
    ,.pll_50_mhz_o(pll_50_mhz_lo));

  // PCIE TOP
  wire [63:0] pcie_data_loopback;
  wire [1:0] pcie_valid_loopback;
  wire [1:0] pcie_yummy_thanks_loopback;

  wire pcie_reset_lo, host_reset_lo;
  wire [31:0] debug_lo;

  bsg_pcie #
    (.NUM_MUX_IO_PORTS_P(2))
  bsg_pcie_inst
    (.clk_i(pll_50_mhz_lo)
    ,.reset_i(pcie_reset_lo)
    ,.data_i(pcie_data_loopback)
    ,.valid_i(pcie_valid_loopback)
    ,.thanks_o(pcie_yummy_thanks_loopback)
    ,.data_o(pcie_data_loopback)
    ,.valid_o(pcie_valid_loopback)
    ,.yummy_i(pcie_yummy_thanks_loopback)
    // PCIE
    ,.PCIE_CLK_P(PCIE_CLK_P) ,.PCIE_CLK_N(PCIE_CLK_N)
    ,.PCIE_PRSNT_B_FPGA(PCIE_PRSNT_B_FPGA)
    ,.PCIE_TX_P(PCIE_TX_P) ,.PCIE_TX_N(PCIE_TX_N)
    ,.PCIE_RX_P(PCIE_RX_P), .PCIE_RX_N(PCIE_RX_N)
    // MISC
    ,.pcie_status_reg_i(32'h0000_ffff)
    ,.host_reset_o(host_reset_lo)
    ,.debug_o(debug_lo));

  assign pcie_reset_lo = FPGA_CPU_RESET_B | host_reset_lo;

  // LED
  assign GPIO_LED = debug_lo[7:0];

endmodule

//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_credit_tracker.v
//
// Author: Michael Taylor
//------------------------------------------------------------

`timescale 1ns / 1ps

`include "bsg_size_fns.v"

module bsg_credit_tracker #
  (parameter num_credits_p  = -1)
  (input clk_i
  ,input reset_i
  ,input yummy_i
  ,input valid_i
  ,output avail_o);  // Does not depend on current valid_i or yummy_i

  // Tricky, but we need to represent one more value than num_credits_p
  // to represent the "no credit" state

  reg [`log2(num_credits_p+1)-1:0] ctr_r, ctr_next;

  // There is space available if the counter is not zero
  assign avail_o = (|ctr_r);

  always @(posedge clk_i)
    if (reset_i == 1'b1)
      ctr_r <= num_credits_p;
    else
      ctr_r <= ctr_next;

  always @(*) begin
    ctr_next = ctr_r;
    case ({yummy_i, valid_i})
      2'b01:
        ctr_next = ctr_r - 1;
      2'b10:
        ctr_next = ctr_r + 1;
      default:
        ctr_next = ctr_r;
    endcase
  end

endmodule

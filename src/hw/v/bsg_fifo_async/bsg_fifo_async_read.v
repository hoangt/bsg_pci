//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_fifo_async_read.v
//
// Note: This module is based on the application note from
//       Sunburst Design, "Simulation and Synthesis Techniques
//       for Asynchronous FIFO Design"
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//          Qiaoshi Zheng - q5zheng@eng.ucsd.edu
//------------------------------------------------------------

module bsg_fifo_async_read #
  (parameter length_width_p = -1)
  (input clk_i
  ,input reset_i
  ,input deq_i
  ,input [length_width_p : 0] ptr_i
  ,output [length_width_p - 1 : 0] addr_o
  ,output [length_width_p : 0] ptr_o
  ,output empty_o);

  // Sync write-pointer with read-clock
  reg [length_width_p : 0] w_ptr_1, w_ptr_2;
  always @(posedge clk_i)
    if(reset_i == 1'b1)
      {w_ptr_2, w_ptr_1} <= 0;
    else
      {w_ptr_2, w_ptr_1} <= {w_ptr_1, ptr_i};

  // Gray/bin pointers
  reg empty_r;
  reg [length_width_p : 0] bin_ptr_r, gray_ptr_r;
  wire [length_width_p : 0] bin_next, gray_next;

  assign bin_next = bin_ptr_r + (deq_i & (~empty_r));
  assign gray_next = (bin_next >> 1) ^ bin_next;

  always @(posedge clk_i)
    if(reset_i == 1'b1)
      {bin_ptr_r, gray_ptr_r} <= 0;
    else
      {bin_ptr_r, gray_ptr_r} <= {bin_next, gray_next};

  // Empty
  always @(posedge clk_i)
    if(reset_i == 1'b1 || gray_next == w_ptr_2)
      empty_r <= 1'b1;
    else
      empty_r <= 1'b0;

  // Outputs
  assign addr_o = bin_ptr_r[length_width_p - 1 : 0];
  assign ptr_o = gray_ptr_r;
  assign empty_o = empty_r;

  // Debug
  always @(posedge clk_i)
    if(deq_i == 1'b1 && empty_r == 1'b1)
      $display("ERROR: Dequeuing empty FIFO %t", $time);

endmodule

//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_fifo_async.v
//
// Note: This module is based on the application note from
//       Sunburst Design, "Simulation and Synthesis Techniques
//       for Asynchronous FIFO Design"
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//          Qiaoshi Zheng - q5zheng@eng.ucsd.edu
//------------------------------------------------------------

module bsg_fifo_async #
  (parameter width_p = -1
  ,parameter length_width_p = -1)
  (input w_clk_i
  ,input w_reset_i
  ,input w_enq_i
  ,input [width_p-1:0] w_data_i
  ,output w_full_o
  ,input r_clk_i
  ,input r_reset_i
  ,output [width_p-1:0] r_data_o
  ,input r_deq_i
  ,output r_empty_o);

  wire full_lo, empty_lo;
  wire [length_width_p : 0] w_ptr_lo, r_ptr_lo;
  wire [length_width_p - 1 : 0] w_addr_lo, r_addr_lo;

  bsg_fifo_async_write #
    (.length_width_p(length_width_p))
  bsg_faw
    (.clk_i(w_clk_i)
    ,.reset_i(w_reset_i)
    ,.enq_i(w_enq_i)
    ,.ptr_i(r_ptr_lo)
    ,.addr_o(w_addr_lo)
    ,.ptr_o(w_ptr_lo)
    ,.full_o(full_lo));

  bsg_fifo_async_mem #
    (.width_p(width_p)
    ,.length_width_p(length_width_p))
  bsg_fm
    (.w_clk_i(w_clk_i)
    ,.w_full_i(full_lo)
    ,.w_enq_i(w_enq_i)
    ,.w_addr_i(w_addr_lo)
    ,.w_data_i(w_data_i)
    ,.r_addr_i(r_addr_lo)
    ,.r_data_o(r_data_o));

  bsg_fifo_async_read #
    (.length_width_p(length_width_p))
  bsg_far
    (.clk_i(r_clk_i)
    ,.reset_i(r_reset_i)
    ,.deq_i(r_deq_i)
    ,.ptr_i(w_ptr_lo)
    ,.addr_o(r_addr_lo)
    ,.ptr_o(r_ptr_lo)
    ,.empty_o(empty_lo));

  // Outputs
  assign w_full_o = full_lo;
  assign r_empty_o = empty_lo;

endmodule

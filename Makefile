#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: Makefile
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#          Qiaoshi Zheng - q5zheng@eng.ucsd.edu
#------------------------------------------------------------

export LM_LICENSE_FILE="YOUR_LICENSE_HERE"
export XILINXD_LICENSE_FILE="YOUR_LICENSE_HERE"

XTCLSH = /path/to/xtclsh
IMPACT_BIN = /path/to/impact

WORK_DIR = $(abspath .)
OUTPUT_DIR = $(WORK_DIR)/out

# Hardware
HW_SRC_DIR = $(WORK_DIR)/src/hw
TOP_NAME = bsg_ml605_top
ISE_FLOW_TCL = $(HW_SRC_DIR)/tcl/ise.tcl
ISE_DIR = $(OUTPUT_DIR)/ise
ISE_LOG = $(ISE_DIR)/ise.log
ML605_IMPACT = $(HW_SRC_DIR)/impact/ml605.impact
CUSTOM_IMPACT = $(ISE_DIR)/custom.impact
BITSTREAM_FILE = $(ISE_DIR)/$(TOP_NAME).bit

export BSG_WORK_DIR=$(WORK_DIR)
export BSG_TOP_NAME=$(TOP_NAME)

# Driver
DRIVER_SRC_DIR = $(WORK_DIR)/src/sw/driver
DRIVER_OUT_DIR = $(OUTPUT_DIR)/driver
DRIVER_KO = $(DRIVER_SRC_DIR)/bsg_pci.ko
KVERSION = $(shell uname -r)

# Host
HOST_SRC_DIR = $(WORK_DIR)/src/sw/host
HOST_CC =\
$(HOST_SRC_DIR)/bsg_pci_host.cc \
$(HOST_SRC_DIR)/main.cc
HOST_OUT_DIR = $(OUTPUT_DIR)/host
HOST_BIN = $(HOST_OUT_DIR)/bsg_pci_loopback


build_hw: $(BITSTREAM_FILE)

$(BITSTREAM_FILE):
	mkdir -p $(ISE_DIR)
	$(XTCLSH) $(ISE_FLOW_TCL) 2>&1 | tee $(ISE_LOG)

download: $(BITSTREAM_FILE)
	sed 's%\<MAKE_ARG_BITSTREAM_FILE\>%\"$(BITSTREAM_FILE)\"%g' $(ML605_IMPACT) > $(CUSTOM_IMPACT)
	$(IMPACT_BIN) -batch $(CUSTOM_IMPACT)

build_driver:
	mkdir -p $(DRIVER_OUT_DIR)
	make -C /lib/modules/$(KVERSION)/build M=$(DRIVER_SRC_DIR) modules
	cp $(DRIVER_KO) $(DRIVER_OUT_DIR)

build_host:
	mkdir -p $(HOST_OUT_DIR)
	g++ -I$(HOST_SRC_DIR) $(HOST_CC) -o $(HOST_BIN)

clean:
	make -C /lib/modules/$(KVERSION)/build M=$(DRIVER_SRC_DIR) clean
	-rm -rf $(OUTPUT_DIR) *.log \
	        $(DRIVER_SRC_DIR)/Module.symvers \
		$(DRIVER_SRC_DIR)/Module.markers \
		$(DRIVER_SRC_DIR)/modules.order \
		_impact* \
		*_dump
